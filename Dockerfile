FROM php:7.4-fpm

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
